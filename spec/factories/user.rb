FactoryGirl.define do
	factory :user do
		sequence(:email, 1000) { |n| "person#{n}@example.com" }
		password "secretsecret"
	end

	factory :invalid_user do
		sequence(:email) {|n| "person#{n}@example"}
		password "test"
	end
end