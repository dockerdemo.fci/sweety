require 'rails_helper'
include Warden::Test::Helpers
Warden.test_mode!

RSpec.feature "Readings", type: :feature do
	user = FactoryGirl.create(:user)

	before(:each) do
		login_as(user, :scope => :user)
	end

  scenario "Create Readings", :js => true do
    visit "/"
  	within("#new_reading") do
  		fill_in "reading_label", with: 100
  		click_on "Enter"
  	end
    wait_for_ajax
    expect(page).to have_content(100)
  end
end
