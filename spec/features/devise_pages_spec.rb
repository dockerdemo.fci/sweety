require 'rails_helper'

RSpec.feature "DevisePages", type: :feature do

	let(:new_user) {FactoryGirl.build(:user)}
	let(:user) {FactoryGirl.create(:user)}

  before(:each) do
    visit root_path
  end

  scenario "user registration" do
  	click_on "Sign up"
  	within("#new_user") do
  		fill_in "Email", with: new_user.email
  		fill_in "Password", with: new_user.password
  		fill_in "Confirm Password", with: new_user.password
  	end
  	click_on "Sign up"
  	expect(page).to have_content("Welcome! You have signed up successfully.")
  end

  scenario "user login" do
  	within("#new_user") do
  		fill_in "Email", with: user.email
  		fill_in "Password", with: user.password
  	end
  	click_on "Log in"
  	expect(page).to have_content("Signed in successfully.")
  end

end
