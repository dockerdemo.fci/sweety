module ReadingsHelper
  def report_types
    {daily: t('readings.report.daily'),month_to_date: t('readings.report.month_to_date'),monthly: t('readings.report.monthly')}
  end
end
