class ReadingsController < ApplicationController

  include ReadingsHelper

  before_action :authenticate_user!
  before_action :readings_of_the_day, except: [:report, :generate_report]
  before_action :reading_instance, only: :destroy

  def new
    @reading = Reading.new
  end

  def create
    @reading = current_user.readings.create(reading_params)
  end

  def destroy
    @reading.destroy
  end

  def report
  end

  def generate_report
    begin
      date = Date.parse(params[:date])
      report_type = params[:report_type]
      if  report_type && date
        case report_type
          when "daily"
            @readings = current_user.readings.this_day(date)
          when "month_to_date"
            @readings = current_user.readings.month_to_date(date)
          when "monthly"
            @readings = current_user.readings.monthly(date)
        end
      end
      respond_to do |format|
        format.js
        format.csv {send_data @readings.to_csv,content_type: 'text/csv', disposition: 'attachment', filename: "#{report_type}_#{date}.csv" }
      end
    rescue
      redirect_to report_readings_path
    end
  end

  private

    def reading_params
      params.require(:reading).permit(:label)
    end

    def readings_of_the_day
      @readings = current_user.readings.this_day(Time.current.to_date)
    end

    def reading_instance
      @reading = Reading.find_by_id(params[:id])
    end
end
