# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

ReportGeneration = ->
  self = this
  
  @viewReport = (reportType, reportDate) ->
    $.ajax
      url: '/readings/generate_report.js'
      method: "GET",
      data:
        report_type: reportType
        date: reportDate
    return

  @setUp = ->
    $('#view_report').on 'click', (e) ->
      e.preventDefault()
      self.viewReport($('#report_type').val(), $('#report_date').val())
      return
    return
  return

report = new ReportGeneration

ready = ->
  $(".datepicker").datepicker(dateFormat: 'dd-mm-yy')
  $("#btn_calendar").click ->
    $(".datepicker").datepicker('show')
  report.setUp()
  return

$(document).ready(ready)
$(document).on('page:load', ready)