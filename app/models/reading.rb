class Reading < ActiveRecord::Base

  MAX_READING_PER_DAY = 4

  #scopes
  default_scope { order(created_at: :desc) }
  scope :this_day, ->(date) {where(created_at: (date.to_date.beginning_of_day..date.to_date.end_of_day))}
  scope :month_to_date, ->(date) {where(created_at: (date.to_date.beginning_of_month.beginning_of_day..date.to_date.end_of_day) )}
  scope :monthly, ->(date) {where(created_at: (date.to_date.beginning_of_month.beginning_of_day..date.to_date.end_of_month.end_of_day))}
  scope :max_label, -> {maximum('label')}
  scope :min_label, -> {minimum('label')}
  scope :avg_label, -> {average('label').round(2)}

  #associations
  belongs_to :user

  #validations
  validates :label, numericality: {only_integer: true, greater_than: 0}, presence: true
  validate :max_reading_per_day, on: :create

  def self.to_csv
    attributes = %w(created_at label)
    CSV.generate(headers: true) do |csv|
      csv << (attributes + %w(maximum minimum average))
      all.each_with_index do |reading, index|
        csv << ( index == 0 ? attributes.map{|attr| reading.send(attr)} + [all.max_label, all.min_label, all.avg_label] :
            attributes.map{|attr| reading.send(attr)})
      end
    end
  end

  private

    def max_reading_per_day
      if user.readings.this_day(Date.today).count >= MAX_READING_PER_DAY
        errors[:base] << I18n.t("active_record.reading.max_reading_per_day", count: MAX_READING_PER_DAY)
      end
    end


end
