Rails.application.routes.draw do
  get 'readings/index'

  devise_for :users
  resources :readings do
    collection do
      get :report
      get :generate_report
    end
  end

  devise_scope :user do
    authenticated do
      root "readings#new"
    end
    unauthenticated do
      root "devise/sessions#new", as: :unauthenticated_root
    end
  end
end
